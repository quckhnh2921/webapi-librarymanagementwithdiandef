﻿namespace LibraryManagement.DTO.AuthorDTO
{
    public class AuthorDTO
    {
        public string Name { get; set; }
    }
}
