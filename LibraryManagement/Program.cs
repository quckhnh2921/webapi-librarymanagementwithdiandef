using LibraryManagement.DataAccess;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
var sqlConnection = builder.Configuration.GetValue<string>("ConnectionStrings:ConnectionSQL");

builder.Services.AddDbContext<AppDbContext>(options => options.UseSqlServer(sqlConnection));
builder.Services.AddControllers();
builder.Services.AddScoped<AppDbContext>();

var app = builder.Build();
app.MapControllers();
app.Run();

