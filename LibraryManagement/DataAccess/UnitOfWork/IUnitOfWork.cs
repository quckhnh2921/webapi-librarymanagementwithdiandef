﻿using LibraryManagement.DataAccess.IRepository;

namespace LibraryManagement.DataAccess.UnitOfWork
{
    public interface IUnitOfWork
    {
        int SaveChange();
        IBookRepository BookRepository { get; }
        IAuthorRepository AuthorRepository { get; }
    }
}
