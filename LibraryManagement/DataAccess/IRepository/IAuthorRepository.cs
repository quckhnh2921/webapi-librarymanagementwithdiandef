﻿using LibraryManagement.Model;

namespace LibraryManagement.DataAccess.IRepository
{
    public interface IAuthorRepository : IGenericRepository<Author>
    {
    }
}
