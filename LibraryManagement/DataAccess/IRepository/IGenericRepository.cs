﻿namespace LibraryManagement.DataAccess.IRepository
{
    public interface IGenericRepository<T> where T : class
    {
        List<T> GetAll();
        T GetByID(int id);
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}
