﻿using LibraryManagement.Model;

namespace LibraryManagement.DataAccess.IRepository
{
    public interface IBookRepository : IGenericRepository<Book>
    {
    }
}
