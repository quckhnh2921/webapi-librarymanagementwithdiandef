﻿using Azure.Core;
using LibraryManagement.DataAccess.IRepository;
using Microsoft.EntityFrameworkCore;

namespace LibraryManagement.DataAccess.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        protected readonly DbSet<T> dbSet;
        public GenericRepository(AppDbContext context) 
        {
            dbSet = context.Set<T>();
        }
        public void Add(T entity)
        {
            var add = dbSet.Add(entity);
            if(add.State != EntityState.Added)
            {
                throw new Exception("Add fail");
            }
            Console.WriteLine("Add successful");
        }

        public void Delete(T entity)
        {
            var remove = dbSet.Remove(entity);
            if (remove.State != EntityState.Deleted)
            {
                throw new Exception("Remove fail");
            }
            Console.WriteLine("Remove successful");
        }

        public List<T> GetAll()
        {
            return dbSet.ToList();
        }

        public T GetByID(int id)
        {
            var enity = dbSet.Find(id);
            if(enity is null)
            {
                throw new Exception($"Can not found by id: {id}");
            }
            return enity;
        }

        public void Update(T entity)
        {
            var update = dbSet.Update(entity);
            if (update.State != EntityState.Modified)
            {
                throw new Exception("Update fail");
            }
            Console.WriteLine("Update successful");
        }
    }
}
