﻿using LibraryManagement.DataAccess.IRepository;
using LibraryManagement.Model;

namespace LibraryManagement.DataAccess.Repository
{
    public class BookRepository : GenericRepository<Book>, IBookRepository
    {
        public BookRepository(AppDbContext context) : base(context)
        {
        }
    }
}
