﻿using LibraryManagement.Model;
using Microsoft.EntityFrameworkCore;
namespace LibraryManagement.DataAccess
{
    public class AppDbContext :DbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Author> Authors { get; set; }

        //Add dependency injection
        public AppDbContext(DbContextOptions<AppDbContext> options) : base (options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Author>()
                        .HasMany(author => author.Books)
                        .WithOne(book => book.Author)
                        .HasForeignKey(book => book.AuthorId);
        }
    }
}
